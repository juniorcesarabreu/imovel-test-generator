/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package imovelgenerator;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Locale;

/**
 *
 * @author Junior Cesar Abreu <juniorcesarabreu@live.com>
 */
public class ImovelGenerator {

    public static final String ENCODE = "windows-1252";

    private static final String STR_REGISTRO = "REGINA MAURA RIBEIRO DIANA                                                                    \r\n" +
"BTRV SILVA RIBEIRO,                                                              53                            CENTRO                                            RIO NOVO                                          RIO NOVO  MG\r\n" +
"CTRV SILVA RIBEIRO,                                                              53                            CENTRO                                            RIO NOVO                                                      0                             30121899\r\n" +
"DConstruído               Particular          Residencial         NãoNão2014\r\n" +
"EEsquina ou Mais de Uma Frente      Irregular           Normal                        Muro      Bem Conservado      \r\n" +
"FCasa                Alinhada            Sup. Frente         Geminada       Avenaria       Telha Colonial      Avenaria       Laje           Pintura             Simples                       Embutida       Táboa               Ótimo          \r\n" +
"G19,26          14        100E      RUA SAO JOSE                                                                    33,03          152       160D      RUA F                                                                           0              0                                                                                                   0              0                                                                                                   0              471,22         195,16         20,25          350,77         \r\n" +
"HIntegrada      SimFornecida      SimSimSimNão2         1         0              \r\n";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        // Writer
        FileOutputStream fos = new FileOutputStream("teste.txt", false);
        OutputStreamWriter osw = new OutputStreamWriter(fos, ENCODE);
        BufferedWriter bw = new BufferedWriter(osw);
        
        for (int i = 0; i < 100000; i++) {
            
            String str = "A" + String.format(Locale.US, "%014d", i) + STR_REGISTRO;
            bw.write(str);

        }

        bw.close();
    }

}
